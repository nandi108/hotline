- Welcome message, eg. Thank you for calling The Bright Path, please hold while we connect you with a teacher. - recommendation if using it is to record as mp3
- How many phone numbers will be included?
- What kind of phone number will the hotline have, eg. the Spanish one is a local landline phone number in Barcelona:
 - Free phone, eg. 800, 1800, 900 (depending on country)
 - a mobile phone number
 - a regional landline phone number in a central city - which city
 - port an existing phone number from another operator
 - search for a phone number that has common digits with an existing phone number
- Do you want to limit length of call? Default limit is 4 hours, at 4 hours the call will be dropped automatically.
- Do you want voicemail in case no one picks the phone? A mp3 recording asking to leave a voicemail is best to use
- What countries will be the phone numbers forwarding to? Multiple countries are supported, eg. Spanish hotline redirects to mobiles in Spain, UK, Finland, and Andorra, note that big countries have cheaper rates while smaller ones like Andorra much higher
- Billing is in USD, pre-paid, minimum top up is $20 with auto-top up supported when balance goes below $10

Example pricing for Spain:
Ascenders/interested people call landline phone number in Barcelona and pay for that part of the call.
The Bright Path Spain pays:
- $1/month fixed price for having a phone number in Barcelona
- $0.0075/min for having an incoming call
- depending on the destination of forwarding:
 - mobile Spain $0.045/min
 - mobile UK $0.045/min
 - mobile Finland $0.065/min
 - mobile Andorra $0.227/min


Options for the future (not supported yet):
- operating hours for the hotline, out of hours says what are the operating hours and goes to voicemail directly
- hold music
